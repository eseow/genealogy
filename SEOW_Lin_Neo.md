# Pasir Panjang Cousins

```mermaid
graph TD
A[Seow Lin Neo]-- son-->B[Tan Sin Lian Essel]
A[Seow Lin Neo]-- son -->C[Tan Sin Seng]
A[Seow Lin Neo]-- son -->D[Tan Sin Whatt]
A[Seow Lin Neo]-- son -->E[Tan Sin Ho]
A[Seow Lin Neo]-- daughter -->F[Tan Geok Eng<br>spinster]
A[Seow Lin Neo]-- daughter -->G[Tan Geok Neo]

C[Tan Sin Seng]-- son -->H[Tan Kim Ann]
C[Tan Sin Seng]-- son -->I[Tan Leong Ann]
C[Tan Sin Seng]-- son -->J[Tan Peng Ann]
C[Tan Sin Seng]-- daughter -->K[Tan Kim Neo]
C[Tan Sin Seng]-- daughter -->L[Tan Kim Lian]

D[Tan Sin Whatt]-- son -->M[Dennis Tan]

E[Tan Sin Ho]-- son -->N[Bernard Tan]
E[Tan Sin Ho]-- daughter -->O[Geok Tan]
E[Tan Sin Ho]-- daughter -->P[May Tan]

G[Tan Geok Neo]-- son -->Q[Clement Chee]
G[Tan Geok Neo]-- son -->R[Cecil Chee]
```