```mermaid
graph TD
A[Seow Chin Teck]-- son -->B[Billy Seow]
A[Seow Chin Teck]-- son -->C[Philip Seow]
A[Seow Chin Teck]-- son -->D[Jimmy Seow]
A[Seow Chin Teck]-- son -->E[Henry Seow]
A[Seow Chin Teck]-- son -->F[Seow Hock Chye Simon]
A[Seow Chin Teck]-- son -->G[Seow Hock Siew Terence]

B[Billy Seow]-- daughter -->H[Agnes Seow Gek Kim]

C[Philip Seow]-- son -->I[Steven Seow]
C[Philip Seow]-- son -->J[David Seow]
C[Philip Seow]-- daughter -->K[Adeline Seow]

D[Jimmy Seow]-- daughter -->L[Jenny Seow]

E[Henry Seow]-- son -->M[Lawrence Seow]
E[Henry Seow]-- daughter -->N[Stella Seow]

F[Seow Hock Chye Simon]-- son -->O[Mark Seow]

G[Seow Hock Siew Terence]-- son -->P[Kenneth Seow]
G[Seow Hock Siew Terence]-- daughter -->Q[Katerina Seow]
```
